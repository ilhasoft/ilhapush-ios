//
//  ISPushSettings.swift
//  Udo
//
//  Created by Daniel Amaral on 26/05/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

open class ISPushSettings: NSObject {

    open static var token: String!
    open static var channel: String!
    open static var url: String!
    open static var handlerURL: String!
    static let preferedLanguageKey = "language"
    static let defaultLanguage = "en"
    static let version1 = "v1/"
    static let version2 = "v2/"
    
    open static var sharedInstance: ISPushSettings!
    
    required public init(token: String, channel: String, url: String, handlerURL: String) {
        super.init()
        ISPushSettings.sharedInstance = self
    }
    
    open class func setConfiguration(_ token: String = "", channel: String,
                                     url: String = "http://push.ilhasoft.mobi/api/",
                                     handlerURL: String = "http://push.ilhasoft.mobi/handlers/gcm/") {
        self.token = token
        self.channel = channel
        self.url = url
        self.handlerURL = handlerURL
        self.init(token: token,
                  channel: channel,
                  url: url,
                  handlerURL: handlerURL)
    }
    
    open class func savePreferedLanguage(_ language: String) {
        let defaults = UserDefaults.standard
        defaults.set(language, forKey: preferedLanguageKey)
        defaults.synchronize()
    }
    
    open class func getPreferedLanguage() -> String {
        guard let preferedLanguage = UserDefaults.standard.object(forKey: preferedLanguageKey) as? String
        else { return defaultLanguage }
        return preferedLanguage
    }
}
