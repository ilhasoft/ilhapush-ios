//
//  URRapidProManager.swift
//  ureport
//
//  Created by Daniel Amaral on 10/09/15.
//  Copyright (c) 2015 ilhasoft. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

protocol ISPushManagerDelegate: class {
    func newMessageReceived(_ message: String)
}

open class ISPushManager: NSObject {
    
    weak var delegate: ISPushManagerDelegate?
    static var sendingAnswers: Bool = false
    
    class func getFlowDefinition(_ flowUuid: String, completion: @escaping (ISPushFlowDefinition) -> Void) {
        guard let baseURL = ISPushSettings.url, let token = ISPushSettings.token else { return }
        let headers = ["Authorization": token]
        let url = "\(baseURL)\(ISPushSettings.version1)flow_definition.json?uuid=\(flowUuid)"

        Alamofire.request(url, method: .get, parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: headers).responseObject { (response: DataResponse<ISPushFlowDefinition>) in
            if let flowDefinition = response.result.value, flowDefinition.entry != nil {
                completion(flowDefinition)
            } else {
                print("Flow definition com estrutura incorreta")
            }
        }
    }
    
    class func getFlowRuns(_ contact: ISPushContact, completion: @escaping ([ISPushFlowRun]?) -> Void) {
        guard let baseURL = ISPushSettings.url, let uuid = contact.uuid,
            let token = ISPushSettings.token else { return }
        let afterDate = ISPushDateUtil.dateFormatterRapidPro(getMinimumDate())
        let url = "\(baseURL)\(ISPushSettings.version1)runs.json?contact=\(uuid)&after=\(afterDate)"
        let headers = ["Authorization": token]
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseObject { (response: DataResponse<ISPushFlowRunResponse>) in
                guard let response = response.result.value else { return }
                (!response.results.isEmpty) ? completion(response.results): completion(nil)
        }
    }
    
    class func getMinimumDate() -> Date {
        let date = Date()
        let gregorian = Calendar(identifier: Calendar.Identifier.gregorian)
        
        var offsetComponents = DateComponents()
        offsetComponents.month = -1
        
        return (gregorian as NSCalendar).date(byAdding: offsetComponents, to: date, options: [])!
    }
    
    class func sendRulesetResponses(_ contact: ISPushContact,
                                    responses: [ISPushRulesetResponse],
                                    completion: @escaping () -> Void) {
        guard let handlerURL = ISPushSettings.handlerURL, let channel = ISPushSettings.channel else { return }
        let token = ISPushSettings.token
        let url = "\(handlerURL)\(channel)"
        let group = DispatchGroup()
        let queue = DispatchQueue(label: "in.ureport-poll-responses", attributes: [])
        self.sendingAnswers = true
        
        for response in responses {
            queue.async(group: group, execute: { () -> Void in
                guard let url = URL(string: url),
                      let tokenValue = token,
                      let pushIdentity = contact.pushIdentity,
                      let response = response.response else { return }
                let request = NSMutableURLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue(tokenValue, forHTTPHeaderField: "Authorization")
                request.timeoutInterval = 15
                
                let postString = "from=\(pushIdentity)&msg=\(response)"
                request.httpBody = postString.data(using: String.Encoding.utf8)
                var httpResponse: URLResponse?
                
                do {
                    try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &httpResponse)
                    print("Sent: \(response)")
                } catch {
                    print("Error on sending poll response")
                }
                Thread.sleep(forTimeInterval: 1.2)
            })
        }
        group.notify(queue: queue) { () -> Void in
            self.sendingAnswers = false
            completion()
        }
    }

    class func sendReceivedMessage(_ contactKey: String, text: String) {
        
        guard let url = ISPushSettings.url, let token = ISPushSettings.token else { return }
        let headers = ["Authorization": token]
        let parameters = [
            "from": contactKey,
            "text": text
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
    
    class func getContactFields(_ completion: @escaping ([String]?) -> Void) {
        guard let url = ISPushSettings.url, let token = ISPushSettings.token else { return }
        let headers = ["Authorization": token]
        Alamofire.request("\(url)\(ISPushSettings.version1)fields.json",
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: headers)
            .responseJSON { (response: DataResponse<Any>) in
                
                guard let response = response.result.value as? NSDictionary else { return }
                var arrayFields: [String] = []
                if let results = response.object(forKey: "results") as? [NSDictionary] {
                    
                    for dictionary in results {
                        if let key = dictionary.object(forKey: "key") as? String {
                            arrayFields.append(key)
                        }
                    }
                }
                completion(arrayFields)
        }
    }
    
    open class func getMessagesFromContact(_ contact: ISPushContact,
                                           completion: @escaping (_ messages: [ISPushMessage]?) -> Void) {
        guard let baseURL = ISPushSettings.url, let uuid = contact.uuid,
            let token = ISPushSettings.token else { return }
        let url = "\(baseURL)\(ISPushSettings.version2)messages.json?contact=\(uuid)"
        let headers = ["Authorization": token]
        
        Alamofire.request(url, method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: headers)
            .responseObject { (response: DataResponse<ISPushMessagesResponse>) in
                guard let response = response.result.value,
                    response.results != nil,
                    !response.results.isEmpty else { return completion(nil) }
                completion(response.results)
        }
    }
    
    open class func getMessageByID(_ messageID: Int, completion: @escaping (_ message: ISPushMessage?) -> Void) {
        guard let baseURL = ISPushSettings.url, let token = ISPushSettings.token else { return }
        let url = "\(baseURL)\(ISPushSettings.version2)messages.json?id=\(messageID)"
        let headers = ["Authorization": token]
        Alamofire.request(url, method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default,
                          headers: headers)
                          .responseObject { (response: DataResponse<ISPushMessagesResponse>) in
            if let response = response.result.value {
                if !response.results.isEmpty {
                    completion(response.results[0])
                } else {
                    completion(nil)
                }
            }
        }
    }

    open class func saveContact(_ contact: ISPushContact,
                                groups: [String],
                                includeCustomFieldsAndValues: [[String: AnyObject]]?,
                                completion: @escaping (_ response: NSDictionary) -> Void) {

        ISPushRapidProContactUtil.buildRapidProContactRootDictionary(contact, groups: groups,
                                includeCustomFieldsAndValues: includeCustomFieldsAndValues) { (rootDicionary) in
            guard let baseURL = ISPushSettings.url,
                  let params = rootDicionary.copy() as? [String: AnyObject],
                  let token = ISPushSettings.token else { return }
            let headers = ["Authorization": token]
            Alamofire.request("\(baseURL)\(ISPushSettings.version1)contacts.json",
                method: .post,
                parameters: params,
                encoding: JSONEncoding.default,
                headers: headers).responseJSON(completionHandler: { (response: DataResponse<Any>) in
                    guard let response = response.result.value as? NSDictionary else { return }
                    completion(response)
            })
        }
    }
}
