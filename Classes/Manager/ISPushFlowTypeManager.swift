//
//  ISPushFlowTypeManager.swift
//  ureport
//
//  Created by John Dalton Costa Cordeiro on 19/11/15.
//  Copyright © 2015 ilhasoft. All rights reserved.
//

import UIKit

class ISPushFlowTypeManager: NSObject {
    
    let openField: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.openField, validation: "true", message: "Please, fill all the fields")
    let choice: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.choice, validation: "contains_any", message: "Please, fill all the fields")
    let openFieldContains: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.openField, validation: "contains", message: "Please, fill all the fields")
    let openFieldNotEmpty: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.openField, validation: "not_empty", message: "Please, fill all the fields")
    let openFieldStarts: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.openField, validation: "starts", message: "Please, fill all the fields")
    let openFieldRegex: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.openField, validation: "regex", message: "Field invalid, check the instructions")
    let number: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.number, validation: "number", message: "Numeric field with invalid value")
    let numberLessThan: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.number, validation: "lt", message: "Numeric field with invalid value, check the instructions")
    let numberGreaterThan: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.number, validation: "gt", message: "Numeric field with invalid value, check the instructions")
    let numberBetween: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.number, validation: "between",
        message: "Numeric field with invalid value, check the instructions")
    let numberEqual: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.number, validation: "eq", message: "Numeric field with invalid value, check the instructions")
    let date: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.date, validation: "date", message: "Date field with invalid value, check the instructions")
    let dateBefore: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.date, validation: "date_before",
        message: "Date field with invalid value, check the instructions")
    let dateAfter: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.date, validation: "date_after", message: "Date field with invalid value, check the instructions")
    let dateEqual: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.date, validation: "date_equal", message: "Date field with invalid value, check the instructions")
    let phone: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.phone, validation: "phone", message: "Phone with invalid value, check the instructions")
    let state: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.state, validation: "state", message: "State with invalid value, check the instructions")
    let district: ISPushFlowTypeValidation = ISPushFlowTypeValidation(type:
        ISPushFlowType.district, validation: "district", message: "District with invalid value, check the instructions")
    let typeValidations: [ISPushFlowTypeValidation]
    
    override init() {
        typeValidations = [openField, choice, openFieldContains, openFieldNotEmpty,
                           openFieldStarts, openFieldRegex, number, numberLessThan,
                           numberGreaterThan, numberBetween, numberEqual, date,
                           dateBefore, dateAfter, dateEqual, phone, state, district]
    }
    
    func getTypeValidationForRule(_ flowRule: ISPushFlowRule) -> ISPushFlowTypeValidation {
        return getTypeValidation((flowRule.test?.type)!)
    }
    
    func getTypeValidation(_ validation: String) -> ISPushFlowTypeValidation {
        for typeValidation in self.typeValidations where typeValidation.validation == validation {
            return typeValidation
        }
        return openField
    }
    
}
