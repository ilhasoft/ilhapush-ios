//
//  URFlowManager.swift
//  ureport
//
//  Created by John Dalton Costa Cordeiro on 17/11/15.
//  Copyright © 2015 ilhasoft. All rights reserved.
//
import Foundation
private func < <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (lhs?, rhs?):
    return lhs < rhs
  case (nil, _?):
    return true
  default:
    return false
  }
}

private func >= <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (lhs?, rhs?):
    return lhs >= rhs
  default:
    return !(lhs < rhs)
  }
}

class ISPushFlowManager {
    
    class func translateFields(_ contact: ISPushContact, message: String) -> String {
        guard let username = contact.name, !username.isEmpty else { return "" }
        return message.replacingOccurrences(of: "@contact", with: username)
    }
    
    class func isFlowActive(_ flowRun: ISPushFlowRun) -> Bool {
        return (flowRun.completed == false && !ISPushFlowManager.isFlowExpired(flowRun))
    }
    
    class func isFlowExpired(_ flowRun: ISPushFlowRun) -> Bool {
        return flowRun.expiredOn != nil && (flowRun.expiresOn.compare(Date()) == ComparisonResult.orderedDescending)
    }
    
    class func isLastActionSet(_ actionSet: ISPushFlowActionSet?) -> Bool {
        return actionSet == nil || actionSet?.destination == nil || actionSet!.destination!.isEmpty
    }
    
    class func hasRecursiveDestination(_ flowDefinition: ISPushFlowDefinition,
                                       ruleSet: ISPushFlowRuleset, rule: ISPushFlowRule) -> Bool {
        if let destination = rule.destination,
           let actionSet = getFlowActionSetByUuid(flowDefinition, destination: destination, currentActionSet: nil),
           let actionSetDestination = actionSet.destination,
           let uuid = ruleSet.uuid {
            
            return actionSetDestination == uuid
        }
        return false
    }
    
    class func getFlowActionSetByUuid(_ flowDefinition: ISPushFlowDefinition,
                                      destination: String?,
                                      currentActionSet: ISPushFlowActionSet?) -> ISPushFlowActionSet? {
        if let actionSets = flowDefinition.actionSets {
            for actionSet in actionSets {
                if let strongDestination = destination, let uuid = actionSet.uuid, strongDestination == uuid {
                    return actionSet
                }
            }
        }
        guard let currentActionSet = currentActionSet,
            let index = flowDefinition.actionSets?.index(where: {$0.uuid == currentActionSet.uuid}),
            flowDefinition.actionSets?.count >= index + 1 else { return nil }
        return flowDefinition.actionSets?[index + 1]
    }
    
    class func getRulesetForAction(_ flowDefinition: ISPushFlowDefinition,
                                   actionSet: ISPushFlowActionSet?) -> ISPushFlowRuleset? {
        guard let ruleSets = flowDefinition.ruleSets else { return nil }
        for ruleSet in ruleSets where (actionSet != nil &&
                                       actionSet?.destination != nil &&
                                       actionSet?.destination == ruleSet.uuid) {
            return ruleSet
        }
        return nil
    }
}
