//
//  ISPushMessage.swift
//  Pods
//
//  Created by Daniel Amaral on 07/06/16.
//
//

import UIKit
import ObjectMapper

public enum ISPushMessageDirection: String {
    case directionIn = "in"
    case directionOut = "Out"
}

public enum ISPushMessageType: String {
    case flow = "flow"
    case inbox = "inbox"
}

open class ISPushMessage: NSObject, Mappable {
    open var id: Int?
    open var direction: String!
    open var type: String!
    open var status: String!
    open var archived: String!
    open var visible: String!
    open var text: String!
    open var ruleset: ISPushFlowRuleset?
    
    public init(msg: String) {
        self.text = msg
        self.direction = ISPushMessageDirection.directionIn.rawValue
    }
    
    public override init() {
        self.direction = ISPushMessageDirection.directionOut.rawValue
    }
    
    required public init?(map: Map) {}
    
    open func mapping(map: Map) {
        self.id  <- map["id"]
        self.direction  <- map["direction"]
        self.type       <- map["type"]
        self.status  <- map["status"]
        self.archived <- map["archived"]
        self.visible <- map["visible"]
        self.text <- map["text"]
        self.ruleset <- map["ruleset"]
    }
}
