//
//  URCountry.swift
//  ureport
//
//  Created by Daniel Amaral on 09/07/15.
//  Copyright (c) 2015 ilhasoft. All rights reserved.
//

import UIKit
import Foundation
private func < <T: Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (lhs?, rhs?):
    return lhs < rhs
  case (nil, _?):
    return true
  default:
    return false
  }
}

enum ISPushCountryCodeType {
    case iso2
    case iso3
}

class ISPushCountry: NSObject {
    
    var code: String?
    var name: String?
    
    init(code: String) {
        self.code = code
        super.init()
    }
    
    override init() {}

    class func getISO2CountryCodeByISO3Code(_ code: String) -> String {
        if let path = Bundle.main.path(forResource: "iso3-country-code", ofType: "json") {
            do {
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path),
                                        options: NSData.ReadingOptions.mappedIfSafe)
                do {
                    let jsonResult = try JSONSerialization.jsonObject(with: jsonData,
                                                          options: JSONSerialization.ReadingOptions.mutableContainers)
                    if let json = jsonResult as? NSDictionary {
                        let filtered = (json).filter({ $0.1 as! String == code })
                        if !filtered.isEmpty, let country = filtered[0].key as? String {
                            return country
                        }
                    }
                } catch _ as NSError {}
            } catch _ as NSError {}
        }
        return ""
    }
}
