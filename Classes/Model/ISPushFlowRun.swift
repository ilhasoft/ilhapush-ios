//
//  URFlowRun.swift
//  ureport
//
//  Created by John Dalton Costa Cordeiro on 17/11/15.
//  Copyright © 2015 ilhasoft. All rights reserved.
//

import UIKit
import ObjectMapper

class ISPushFlowRun: NSObject, Mappable {
    
    var flowUuid: String!
    var flow: Int!
    var completed: Bool!
    var expiresOn: Date!
    var expiredOn: Date!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        self.flowUuid  <- map["flow_uuid"]
        self.flow       <- map["flow"]
        self.completed  <- map["completed"]
        self.expiresOn <- (map["expires_on"], ISPushRapidPRODateTransform())
        self.expiredOn <- (map["expired_on"], ISPushRapidPRODateTransform())
    }
}
