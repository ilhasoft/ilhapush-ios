//
//  ISPushRapidProContactUtil.swift
//  ureport
//
//  Created by Daniel Amaral on 15/01/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class ISPushRapidProContactUtil: NSObject {
    static var rapidProContact = NSMutableDictionary()
    static var groupList: [String] = []
    
    class func putValueIfExists(_ value: String?, countryProgramContactFields: [String], possibleFields: [String]) {
        
        if value == nil || value?.count == 0 {
            return
        }
        
        for possibleField in possibleFields {
            let index = countryProgramContactFields.index(of: possibleField)
            if let indexValue = index, indexValue != -1 {
                let field = countryProgramContactFields[indexValue]
                ISPushRapidProContactUtil.rapidProContact.setValue(value, forKey: field)
                break
            }
        }
    }
    
    class func buildRapidProContactDictionaryWithContactFields(_ contact: ISPushContact,
                                                               completion: @escaping (NSDictionary) -> Void) {
        
        ISPushManager.getContactFields { (contactFields: [String]?) in
            
            if let contactFields = contactFields, !contactFields.isEmpty {
                var age = 0
                if contact.birthday != nil,
                    let timeInterval = NSNumber(value: contact.birthday.doubleValue/1000 as Double) as? TimeInterval {
                    if let year = (Calendar.current as NSCalendar)
                        .components(.year, from: Date(timeIntervalSince1970: timeInterval),
                                    to: Date(), options: []).year {
                        age = year
                    }
                    ISPushRapidProContactUtil.putValueIfExists(ISPushDateUtil.birthDayFormatterRapidPro(
                        Date(timeIntervalSince1970: timeInterval), brFormat: false),
                                                countryProgramContactFields: contactFields,
                                                possibleFields: ["birthday", "birthdate", "birth_day", "date_of_birth"])
                    ISPushRapidProContactUtil.putValueIfExists(String(age),
                                                countryProgramContactFields: contactFields, possibleFields: ["age"])
                    ISPushRapidProContactUtil.putValueIfExists(String(ISPushDateUtil.getYear(
                        Date(timeIntervalSince1970: timeInterval))), countryProgramContactFields: contactFields,
                                                                     possibleFields: ["year_of_birth", "born"])
                }
                ISPushRapidProContactUtil.putValueIfExists(contact.email,
                                                           countryProgramContactFields: contactFields,
                                                           possibleFields: ["email", "e_mail"])
                ISPushRapidProContactUtil.putValueIfExists(contact.name,
                                                           countryProgramContactFields: contactFields,
                                                           possibleFields: ["nickname", "nick_name"])
                ISPushRapidProContactUtil.putValueIfExists(contact.gender,
                                                           countryProgramContactFields: contactFields,
                                                           possibleFields: ["gender"])
                ISPushRapidProContactUtil.putValueIfExists(contact.state,
                                                           countryProgramContactFields: contactFields,
                                                           possibleFields: ["state", "region", "province", "county"])
                completion(ISPushRapidProContactUtil.rapidProContact)
            }
        }
    }
    
    class func buildRapidProContactRootDictionary(_ contact: ISPushContact,
                                                  groups: [String],
                                                  includeCustomFieldsAndValues: [[String: AnyObject]]?,
                                                  completion: (_ rootDicionary: NSDictionary) -> Void) {
        let rootDictionary = NSMutableDictionary()
        rootDictionary.setValue(ISPushRapidProContactUtil.rapidProContact, forKey: "fields")
        if let pushIdentity = contact.pushIdentity {
            rootDictionary.setValue(["gcm:\(pushIdentity)"], forKey: "urns")
        }
        rootDictionary.setValue(contact.name, forKey: "name")
        if let includeCustomFieldsAndValues = includeCustomFieldsAndValues {
            for dictionary in includeCustomFieldsAndValues {
                for (_, (key: key, value: value)) in dictionary.enumerated() {
                    ISPushRapidProContactUtil.rapidProContact.setValue(String(describing: value), forKey: key)
                }
            }
        }

        if !groups.isEmpty {
            rootDictionary.setValue(groups, forKey: "groups")
        }
        completion(rootDictionary)
    }
    
    class func addGenderGroup(_ contact: ISPushContact) {
        // Do something
    }
    
    class func addAgeGroup(_ contact: ISPushContact) {
        // Do something
    }
}
