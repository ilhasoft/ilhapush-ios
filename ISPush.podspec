#
# Be sure to run `pod lib lint ISPush.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ISPush"
  s.version          = "0.0.2"
  s.summary          = "IlhaPush"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
                       Lib to chat with costumers
                       DESC

  s.homepage         = "https://bitbucket.org/ilhasoft/ilhapush-ios"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Daniel" => "daniel@ilhasoft.com.br" }
  s.source       = { :git => "https://bitbucket.org/ilhasoft/ilhapush-ios.git", :tag => "0.0.1" }
  s.social_media_url   = "https://twitter.com/danielamarall"

  s.ios.deployment_target = '9.0'

  s.source_files = 'Classes/**/*'
  s.resources = 'Resources/*'

  #s.resource_bundles = {
  #  'Udo' => ['Ilha/Assets/*.png']
  #}

  #s.public_header_files = 'Pod/Classes/**/*.h'

    s.dependency 'AlamofireObjectMapper', '4.0.0'
    s.dependency 'Alamofire', '4.0.1'
    s.dependency 'MDHTMLLabel', '1.0.2'
    s.dependency 'ISScrollViewPageSwift'
    s.dependency 'MBProgressHUD', '1.0.0'

end

