# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.3] - 2017-08-25

### Added
- Added default values to ISPushChatViewController constructor parameters in other to improve retrocompatibility.

### Changed
- Enhacements on showing and hidding keyboard with or without TabBar in ISPushChatViewController.

## [0.0.2] - 2017-07-10
### Added
- Throwing NSNotification when an answer is tapped. The notification carries the text of the button tapped. The purpose here is to allow the developer to create custom interactions accordingly with user's choices;

### Changed
- Dismissing Keyboard when user tapped an options;
- Dismissing Keyboard when touching the screen;
- Improved animation for text field when keyboard is displayed or hidden.