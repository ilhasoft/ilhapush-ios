#
# Be sure to run `pod lib lint Udo.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "Udo"
  s.version          = "0.0.1"
  s.summary          = "Udo Flow"

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
                       Lib to run flow
                       DESC

  s.homepage         = "https://bitbucket.org/ilhasoft/udo-ios"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Daniel" => "daniel@ilhasoft.com.br" }
  s.source       = { :git => "https://bitbucket.org/ilhasoft/udo-ios.git", :tag => "0.0.1" }
  s.social_media_url   = "https://twitter.com/danielamarall"

  s.ios.deployment_target = '8.0'

  s.source_files = 'Classes/**/*'
  s.resources = 'Resources/*'

  #s.resource_bundles = {
  #  'Udo' => ['Udo/Assets/*.png']
  #}

  #s.public_header_files = 'Pod/Classes/**/*.h'
    s.dependency 'AlamofireObjectMapper'
    s.dependency 'Alamofire'
    s.dependency 'IlhasoftCore'
    s.dependency 'MDHTMLLabel'
    s.dependency 'ProgressHUD'

end

